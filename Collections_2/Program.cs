﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Collections.Concurrent;

namespace Collections_2
{
    class Program
    {
        static void Main(string[] args)
        {
            //here is the ConcurrentBag . It's got nothing special to do with multi threading as such
            //you add and remove, just like that

            //the important thing is, the list itself is thread safe. 

            //as of now, I dont see what is so special about the bag

            //creating a simple collection of type string 
            ConcurrentBag<string> temp_bag = new ConcurrentBag<string>();

            //adding some items

            temp_bag.Add("hello there");
            temp_bag.Add("hello there 2");
            temp_bag.Add("hello there 3");
            temp_bag.Add("hello there 4");
            temp_bag.Add("hello there 5");

            string result_1;

            //before removal of items with Try Take
            Console.WriteLine("List as it was before items were removed");

            int i = 0;
            foreach (string s in temp_bag)
            {
                Console.WriteLine("{0} - {1}", i++, s);
            }

            //takes the elment out of the list and puts it in result_1
            temp_bag.TryTake(out result_1);
            Console.WriteLine("{0} was removed from the list", result_1);

            //takes the elment out of the list and puts it in result_1. same thing again
            temp_bag.TryTake(out result_1);
            Console.WriteLine("{0} was removed from the list", result_1);

            //checking if an element is there to remove
            temp_bag.TryPeek(out result_1);
            Console.WriteLine("{0} is availabel to remove from the list", result_1);

            //after removal of items with Try Take
            Console.WriteLine("List as it is after items were removed");

            i = 0;
            foreach (string s in temp_bag)
            {
                Console.WriteLine("{0} - {1}", i++, s);
            }


            //lets try the stack and the queu collections

            Console.WriteLine("now showing the stack stuff");

            ConcurrentStack<string> temp_stack = new ConcurrentStack<string>();

            //let me add or 'push' some items into this stack
            //just remember that stack is last in first out

            temp_stack.Push("push hello there 1");
            temp_stack.Push("push hello there 2");
            temp_stack.Push("push hello there 3");
            temp_stack.Push("push hello there 4");
            temp_stack.Push("push hello there 5");
            temp_stack.Push("push hello there 6");

            //displaying before popping. man, i almost wrote pooping :P

            Console.WriteLine("Stack List as it was before items were removed. Note that that last item to be pushed appears at the top of the list");

            i = 0;
            foreach (string s in temp_stack)
            {
                Console.WriteLine("{0} - {1}", i++, s);
            }

            //lets pop some items now

            temp_stack.TryPop(out result_1);
            Console.WriteLine("{0} was popped", result_1);

            temp_stack.TryPop(out result_1);
            Console.WriteLine("{0} was popped", result_1);

            Console.WriteLine("Stack List as it was after items were removed");

            i = 0;
            foreach (string s in temp_stack)
            {
                Console.WriteLine("{0} - {1}", i++, s);
            }

            //now lets do the queue

            Console.WriteLine("This is the queue ");

            //creating that queue list
            //remember that queue is like stack but with first in first out 
            ConcurrentQueue<string> queue_temp = new ConcurrentQueue<string>();

            //lets add some items
            queue_temp.Enqueue("queue hello there 1");
            queue_temp.Enqueue("queue hello there 2");
            queue_temp.Enqueue("queue hello there 3");
            queue_temp.Enqueue("queue hello there 4");
            queue_temp.Enqueue("queue hello there 5");

            //displaying the queue before I kick some items out of it
            Console.WriteLine("This is the queue before I remove some items out of it");

            i = 0;
            foreach(string s in queue_temp)
            {
                Console.WriteLine("{0} - {1}",i++,s);
            }

            //removing a few items
            queue_temp.TryDequeue(out result_1);
            Console.WriteLine("{0} was removed.",result_1);
            queue_temp.TryDequeue(out result_1);
            Console.WriteLine("{0} was removed.", result_1);

            //displaying the queue after I kick some items out of it
            Console.WriteLine("This is the queue after I removed some items out of it");

            i = 0;
            foreach (string s in queue_temp)
            {
                Console.WriteLine("{0} - {1}", i++, s);
            }

            //this is the dictionary collection
            //dictionary works on the key - value pair
            //I can add new items, delete items and of course update existing items

            Console.WriteLine("now, the dictionary collection");

            //my key and value are both string. You can use int and string or int and int or any combination of types that would help you
            ConcurrentDictionary<string, string> temp_dictionary = new ConcurrentDictionary<string, string>();

            //I will add some items.
            temp_dictionary.TryAdd("one", "hello dictionary 1");
            temp_dictionary.TryAdd("two", "hello dictionary 2");
            temp_dictionary.TryAdd("three", "hello dictionary 3");
            temp_dictionary.TryAdd("four", "hello dictionary 4");
            temp_dictionary.TryAdd("five", "hello dictionary 5");

            //let me display this dicitionary before i remove some items and update some
            Console.WriteLine("dictionary collection before it was modified");

            foreach (KeyValuePair<string,string> s in temp_dictionary)
            {
                Console.WriteLine("Key is {0} - Value is {1}",s.Key,s.Value);
            }

            //removing an item
            temp_dictionary.TryRemove("three", out result_1);
            Console.WriteLine("{0} with the key {1} removed",result_1,"three");
            temp_dictionary.TryUpdate("four", "hello dictionary 4 updated", "hello dictionary 4");
            Console.WriteLine("item with the key {0} updated with new value", "four");

            //updating an item

            //let me display this dicitionary after I changed some items
            Console.WriteLine("dictionary collection after it was modified");

            foreach (KeyValuePair<string, string> s in temp_dictionary)
            {
                Console.WriteLine("Key is {0} - Value is {1}", s.Key, s.Value);
            }

            //lets stop the console from dissapearing
            Console.ReadLine();

        }
    }
}
